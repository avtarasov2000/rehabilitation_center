package testgroup.rehabcenter.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import testgroup.rehabcenter.model.Employee;
import testgroup.rehabcenter.service.EmployeeService;


import java.util.List;


@Controller
public class EmployeeController {

    private Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private int page;
    private EmployeeService employeeService;


    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/",  method = RequestMethod.GET)
    public ModelAndView employers(@RequestParam(defaultValue = "1") int page) {
        List<Employee> employers = employeeService.allEmployers(page);

        this.page = page;
        int employersCount = employeeService.employersCount();
        int pagesCount = (employersCount + 19)/20;

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("table");
        modelAndView.addObject("employers", employers);
        modelAndView.addObject("page", page);
        modelAndView.addObject("employersCount", employersCount);
        modelAndView.addObject("pagesCount", pagesCount);

        return modelAndView;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView editPage(@PathVariable int id) {
        Employee employee = employeeService.getById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("modification");
        modelAndView.addObject("employee", employee);
        return modelAndView;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView editEmployee(@ModelAttribute("employee") Employee employee) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/?page="+page);
        employeeService.edit(employee);

        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("modification");
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addEmployee(@ModelAttribute("employee") Employee employee) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        employeeService.add(employee);
        return modelAndView;
    }

    @RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
    public ModelAndView deleteEmployee(@PathVariable("id") int id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/");
        Employee employee = employeeService.getById(id);
        employeeService.delete(employee);
        return modelAndView;
    }

//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public ModelAndView loginPage(){
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("login");
//        return modelAndView;
//    }
//
//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    public ModelAndView login(@ModelAttribute("employee") Employee employee, @ModelAttribute("speciality") String speciality) {
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("redirect:/");
//        employeeService.add(employee);
//
////        for (String str :
////                speciality) {
////        System.out.println(speciality);
////        Speciality spec = specialityService.getByName(speciality);
////        tieService.add(new DocSpecialityTie(employee.getId(),spec.getId(),employee,spec));
////        }
//
//        return modelAndView;
//    }

//    private Speciality getSpeciality(String spec, List<Speciality> specialities){
//        for (Speciality spc:
//             specialities) {
//            if (spc.getSpeciality().equals(spec)){
//                return spc;
//            }
//        }
//        return null;
//    }

}