package testgroup.rehabcenter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import testgroup.rehabcenter.model.Appointment;
import testgroup.rehabcenter.model.Diagnosis;
import testgroup.rehabcenter.model.Patient;
import testgroup.rehabcenter.service.PatientService;

import java.util.List;

@Controller
public class PatientController {

    private PatientService patientService;
    private int page;

    @Autowired
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @RequestMapping(value = "/patients/{id}",  method = RequestMethod.GET)
    public ModelAndView patients(@PathVariable("id") int id) {
        List<Patient> patients = patientService.allPatientsByDoctorsId(id);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("patients");
        modelAndView.addObject("patients", patients);
        return modelAndView;
    }

    @RequestMapping(value = "/appointments/{id}",  method = RequestMethod.GET)
    public ModelAndView appointments(@PathVariable("id") int id) {
        List<Appointment> appointments = patientService.getAppointments(id);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("appointments");
        modelAndView.addObject("appointments", appointments);
        return modelAndView;
    }

    @RequestMapping(value = "/diagnosis/{id}",  method = RequestMethod.GET)
    public ModelAndView diagnosis(@PathVariable("id") int id) {
        List<Diagnosis> diagnosis = patientService.getDiagnosis(id);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("diagnosis");
        modelAndView.addObject("diagnosis", diagnosis);
        return modelAndView;
    }
//    @RequestMapping(value = "/addPatient", method = RequestMethod.GET)
//    public ModelAndView addPage() {
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("modification");
//        return modelAndView;
//    }
//
//    @RequestMapping(value = "/addPatient", method = RequestMethod.POST)
//    public ModelAndView addEmployee(@ModelAttribute("patient") Patient patient) {
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("redirect:/patients/"+patient.getDoctors_id().intValue());
//        patientService.add(patient);
//        return modelAndView;
//    }

}
