package testgroup.rehabcenter.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import testgroup.rehabcenter.model.Appointment;

import java.util.List;

@Repository
public class AppointDaoImpl implements AppointmentDao {
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Appointment> getAllFromPatientId(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Appointment where patientId=:id",Appointment.class).setParameter("id", id).list();
    }
}
