package testgroup.rehabcenter.dao;

import testgroup.rehabcenter.model.Appointment;

import java.util.List;

public interface AppointmentDao {
    public List<Appointment> getAllFromPatientId(int id);
}
