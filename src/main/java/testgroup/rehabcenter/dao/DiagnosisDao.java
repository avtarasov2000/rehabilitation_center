package testgroup.rehabcenter.dao;

import testgroup.rehabcenter.model.Diagnosis;

import java.util.List;

public interface DiagnosisDao{
    public List<Diagnosis> getAllFromPatientId(int id);
}
