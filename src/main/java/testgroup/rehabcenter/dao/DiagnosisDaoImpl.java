package testgroup.rehabcenter.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import testgroup.rehabcenter.model.Diagnosis;

import java.util.List;

@Repository
public class DiagnosisDaoImpl implements DiagnosisDao{
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    @Override
    public List<Diagnosis> getAllFromPatientId(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Diagnosis where id_patient=:id", Diagnosis.class).setParameter("id", id).list();
    }
}
