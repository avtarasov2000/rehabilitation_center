package testgroup.rehabcenter.dao;

import testgroup.rehabcenter.model.DocSpecialityTie;

import java.util.List;

public interface DocSpecialityTieDao {
    List<DocSpecialityTie> allTies();
    void add(DocSpecialityTie tie);
    void delete(DocSpecialityTie tie);
    void edit(DocSpecialityTie tie);
    DocSpecialityTie getById(int id);
}
