package testgroup.rehabcenter.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import testgroup.rehabcenter.model.DocSpecialityTie;

import java.util.List;
@Repository
public class DocSpecialityTieDaoImpl implements DocSpecialityTieDao{

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<DocSpecialityTie> allTies() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from DocSpecialityTie", DocSpecialityTie.class).list();
    }

    @Override
    public void add(DocSpecialityTie tie) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(tie);
    }

    @Override
    public void delete(DocSpecialityTie tie) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(tie);
    }

    @Override
    public void edit(DocSpecialityTie tie) {
        Session session = sessionFactory.getCurrentSession();
        session.update(tie);
    }

    @Override
    public DocSpecialityTie getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(DocSpecialityTie.class,id);
    }
}
