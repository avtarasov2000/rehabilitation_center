package testgroup.rehabcenter.dao;

import testgroup.rehabcenter.model.Employee;

import java.util.List;

public interface EmployeeDao {
    List<Employee> allEmployers(int page);
    void add(Employee employee);
    void delete(Employee employee);
    void edit(Employee employee);
    Employee getById(int id);
    int employersCount();

}
