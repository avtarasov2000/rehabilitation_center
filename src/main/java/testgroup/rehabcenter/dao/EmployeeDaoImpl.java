package testgroup.rehabcenter.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import testgroup.rehabcenter.model.Employee;

import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int employersCount() {
        Session session = sessionFactory.getCurrentSession();
//        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("select count(*) from Employee", Number.class).getSingleResult().intValue();
    }

    @Override
    public List<Employee> allEmployers(int page) {
        Session session = sessionFactory.getCurrentSession();
//        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return session.createQuery("from Employee ", Employee.class).setFirstResult(20 * (page - 1)).setMaxResults(20).list();
    }

    @Override
    public void add(Employee employee) {
//        Session session = sessionFactory.openSession();
//        Transaction tx1 = session.beginTransaction();
//        session.persist(employee);
//        tx1.commit();
//        session.close();
        Session session = sessionFactory.getCurrentSession();
        session.persist(employee);
    }

    @Override
    public void delete(Employee employee) {
//        Session session = sessionFactory.openSession();
//        Transaction tx1 = session.beginTransaction();
//        session.delete(employee);
//        tx1.commit();
//        session.close();
        Session session = sessionFactory.getCurrentSession();
        session.delete(employee);
    }

    @Override
    public void edit(Employee employee) {
//        Session session = sessionFactory.openSession();
//        Transaction tx1 = session.beginTransaction();
//        session.update(employee);
//        tx1.commit();
//        session.close();
        Session session = sessionFactory.getCurrentSession();
        session.update(employee);
    }

    @Override
    public Employee getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Employee.class, id);
    }
}
