package testgroup.rehabcenter.dao;

import testgroup.rehabcenter.model.Patient;

import java.util.List;

public interface PatientDao{
    List<Patient> allPatients(int page);
    List<Patient> allPatientsByDoctorsId(int page);
    void add(Patient patient);
    void delete(Patient patient);
    void edit(Patient patient);
    Patient getById(int id);
    int patientsCount();
}
