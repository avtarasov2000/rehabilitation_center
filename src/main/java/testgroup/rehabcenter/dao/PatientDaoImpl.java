package testgroup.rehabcenter.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import testgroup.rehabcenter.model.Patient;

import java.util.List;

@Repository
public class PatientDaoImpl implements PatientDao{

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List <Patient> allPatientsByDoctorsId(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Patient where doctors_id=:id",Patient.class).setParameter("id",id).list();
    }

    @Override
    public List<Patient> allPatients(int page) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Patient ", Patient.class).setFirstResult(20 * (page - 1)).setMaxResults(20).list();
    }

    @Override
    public void add(Patient patient) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(patient);
    }

    @Override
    public void delete(Patient patient) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(patient);
    }

    @Override
    public void edit(Patient patient) {
        Session session = sessionFactory.getCurrentSession();
        session.update(patient);
    }

    @Override
    public Patient getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Patient.class, id);
    }

    @Override
    public int patientsCount() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select count(*) from Patient", Number.class).getSingleResult().intValue();
    }
}
