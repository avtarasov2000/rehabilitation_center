package testgroup.rehabcenter.dao;

import testgroup.rehabcenter.model.Speciality;

import java.util.List;

public interface SpecialityDao{
    List<Speciality> allSpecialitys();
    void add(Speciality speciality);
    void delete(Speciality speciality);
    void edit(Speciality speciality);
    Speciality getById(int id);
    Speciality getByName(String name);
}