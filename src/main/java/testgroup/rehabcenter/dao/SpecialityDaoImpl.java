package testgroup.rehabcenter.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import testgroup.rehabcenter.model.Speciality;

import java.util.List;

@Repository
public class SpecialityDaoImpl implements SpecialityDao{

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Speciality> allSpecialitys() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Speciality", Speciality.class).list();
    }

    @Override
    public void add(Speciality speciality) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(speciality);
    }

    @Override
    public void delete(Speciality speciality) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(speciality);
    }

    @Override
    public void edit(Speciality speciality) {
        Session session = sessionFactory.getCurrentSession();
        session.update(speciality);
    }

    @Override
    public Speciality getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Speciality.class, id);
    }

    @Override
    public Speciality getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("select * from Speciality WHERE speciality="+name, Speciality.class).getSingleResult();

    }
}
