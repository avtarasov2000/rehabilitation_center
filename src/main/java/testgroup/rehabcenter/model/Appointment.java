package testgroup.rehabcenter.model;

import javax.persistence.*;

@Entity
@Table(name = "appointments")
public class Appointment{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "type")
    private String type;
    @Column(name = "pattern_per_day")
    private Integer pattern;
    @Column(name = "period")
    private Integer period;
    @Column(name = "dose")
    private Double dose;
    @Column(name = "patient_id")
    private Integer patientId;
    @ManyToOne (optional=false, cascade=CascadeType.ALL)
    @JoinColumn (name="patient_id", insertable = false, updatable = false)
    private Patient patient;

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPattern() {
        return pattern;
    }

    public void setPattern(Integer pattern) {
        this.pattern = pattern;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Double getDose() {
        return dose;
    }

    public void setDose(Double dose) {
        this.dose = dose;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
