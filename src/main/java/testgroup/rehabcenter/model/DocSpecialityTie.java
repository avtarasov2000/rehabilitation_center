package testgroup.rehabcenter.model;

import javax.persistence.*;

@Entity
@Table(name = "job")
public class DocSpecialityTie{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "employee_id")
    private Integer doctor;
    @Column(name = "specialitys_id")
    private Integer speciality;

    @ManyToOne (optional=false, cascade=CascadeType.ALL)
    @JoinColumn (name="employee_id", insertable = false, updatable = false)
    private Employee employee;
    @ManyToOne(optional=false, cascade=CascadeType.ALL)
    @JoinColumn(name = "specialitys_id", insertable = false, updatable = false)
    private Speciality spec;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Speciality getSpec() {
        return spec;
    }

    public void setSpec(Speciality spec) {
        this.spec = spec;
    }


    public DocSpecialityTie() {
    }

    public DocSpecialityTie(int doctor, int speciality, Employee employee, Speciality spec) {
        this.doctor = doctor;
        this.speciality = speciality;
        this.employee = employee;
        this.spec = spec;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctors) {
        this.doctor = doctors;
    }

    public int getSpeciality() {
        return speciality;
    }

    public void setSpeciality(int speciality) {
        this.speciality = speciality;
    }

//    @Override
//    public String toString() {
//        return spec.getSpeciality();
//    }
}
