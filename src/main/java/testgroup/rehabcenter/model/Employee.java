package testgroup.rehabcenter.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
@Table(name = "employers")
public class Employee {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id ;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SURNAME")
    private String surname;
    @Column(name = "SECOND_NAME")
    private String secondName;
    @Column(name = "BIRTHDAY")
    private Date birthday;
    @OneToMany(mappedBy = "employee",fetch = FetchType.LAZY)
    private Collection<DocSpecialityTie> ties;
    @OneToMany(mappedBy = "doctor",fetch = FetchType.LAZY)
    private Collection<Patient> patients;


    public void setId(Integer id) {
        this.id = id;
    }

    public Collection <Patient> getPatients() {
        return patients;
    }

    public void setPatients(Collection <Patient> patients) {
        this.patients = patients;
    }

    public void setTies(Collection <DocSpecialityTie> ties) {
        this.ties = ties;
    }

    public Employee() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", secondName='" + secondName + '\'' +
                ", birthday=" + birthday +
                ", id=" + id +
                '}';
    }


    public Collection <DocSpecialityTie> getTies() {
        return ties;
    }
}
