package testgroup.rehabcenter.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "Patients")
public class Patient{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "NAME")
    private String name;
    @Column(name = "SURNAME")
    private String surname;
    @Column(name = "SECOND_NAME")
    private String secondName;
    @Column(name = "insurance")
    private Integer insurance;
    @Column(name = "status")
    private String status;

    @Column(name = "doctor")
    private Integer doctors_id;
    @ManyToOne (optional=false, cascade=CascadeType.ALL)
    @JoinColumn (name="doctor", insertable = false, updatable = false)
    private Employee doctor;
    @OneToMany(mappedBy = "patient",fetch = FetchType.LAZY)
    private Collection<Diagnosis> diagnosises;
    @OneToMany(mappedBy = "patient",fetch = FetchType.LAZY)
    private Collection<Appointment> appointments;


    public Integer getDoctors_id() {
        return doctors_id;
    }

    public void setDoctors_id(Integer doctors_id) {
        this.doctors_id = doctors_id;
    }

    public Collection <Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(Collection <Appointment> appointments) {
        this.appointments = appointments;
    }
    public Collection <Diagnosis> getDiagnosises() {
        return diagnosises;
    }

    public void setDiagnosises(Collection <Diagnosis> diagnosises) {
        this.diagnosises = diagnosises;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Integer getInsurance() {
        return insurance;
    }

    public void setInsurance(Integer insurance) {
        this.insurance = insurance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Employee getDoctor() {
        return doctor;
    }

    public void setDoctor(Employee doctor) {
        this.doctor = doctor;
    }
}
