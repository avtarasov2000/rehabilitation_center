package testgroup.rehabcenter.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "specialitys")
public class Speciality {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "speciality", unique = true)
    private String speciality;

    @OneToMany(mappedBy = "employee",fetch = FetchType.LAZY)
    private Collection<DocSpecialityTie> ties;



    public Collection <DocSpecialityTie> getTies() {
        return ties;
    }

    public void setTies(Collection <DocSpecialityTie> ties) {
        this.ties = ties;
    }

    public Speciality() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

}
