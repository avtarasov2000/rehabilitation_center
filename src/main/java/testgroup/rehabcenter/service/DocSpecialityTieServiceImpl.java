package testgroup.rehabcenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import testgroup.rehabcenter.dao.DocSpecialityTieDao;
import testgroup.rehabcenter.model.DocSpecialityTie;

import java.util.List;
@Service
public class DocSpecialityTieServiceImpl implements DocSpecialityTieService{

    private DocSpecialityTieDao docSpecialityTieDao;

    @Autowired
    public void setDocSpecialityTieDao(DocSpecialityTieDao docSpecialityTieDao) {
        this.docSpecialityTieDao = docSpecialityTieDao;
    }

    @Override
    @Transactional
    public List<DocSpecialityTie> allTies() {
        return docSpecialityTieDao.allTies();
    }

    @Override
    @Transactional
    public void add(DocSpecialityTie tie) {
        docSpecialityTieDao.add(tie);
    }

    @Override
    @Transactional
    public void delete(DocSpecialityTie tie) {
        docSpecialityTieDao.delete(tie);
    }

    @Override
    @Transactional
    public void edit(DocSpecialityTie tie) {
        docSpecialityTieDao.edit(tie);
    }

    @Override
    @Transactional
    public DocSpecialityTie getById(int id) {
        return docSpecialityTieDao.getById(id);
    }
}
