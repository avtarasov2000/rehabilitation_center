package testgroup.rehabcenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import testgroup.rehabcenter.dao.EmployeeDao;
import testgroup.rehabcenter.model.Employee;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    private EmployeeDao employeeDao;


    @Autowired
    public void setEmployeeDao(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    @Transactional
    public List<Employee> allEmployers(int page) {
        return employeeDao.allEmployers(page);
    }

    @Override
    @Transactional
    public void add(Employee employee) {
        employeeDao.add(employee);
    }

    @Override
    @Transactional
    public int employersCount() {
        return employeeDao.employersCount();
    }

    @Override
    @Transactional
    public void delete(Employee employee) {
        employeeDao.delete(employee);
    }

    @Override
    @Transactional
    public void edit(Employee employee) {
        employeeDao.edit(employee);
    }

    @Override
    @Transactional
    public Employee getById(int id) {
        return employeeDao.getById(id);
    }
}
