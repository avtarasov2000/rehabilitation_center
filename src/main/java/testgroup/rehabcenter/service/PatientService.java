package testgroup.rehabcenter.service;

import testgroup.rehabcenter.model.Appointment;
import testgroup.rehabcenter.model.Diagnosis;
import testgroup.rehabcenter.model.Patient;

import java.util.List;

public interface PatientService{
    List<Patient> allPatients(int id);
    List<Patient> allPatientsByDoctorsId(int page);
    void add(Patient patient);
    void delete(Patient patient);
    void edit(Patient patient);
    Patient getById(int id);
    int patientsCount();
    public List<Appointment> getAppointments(int id);
    public List<Diagnosis> getDiagnosis(int id);

}
