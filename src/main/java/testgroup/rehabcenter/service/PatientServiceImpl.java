package testgroup.rehabcenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import testgroup.rehabcenter.dao.AppointmentDao;
import testgroup.rehabcenter.dao.DiagnosisDao;
import testgroup.rehabcenter.dao.PatientDao;
import testgroup.rehabcenter.model.Appointment;
import testgroup.rehabcenter.model.Diagnosis;
import testgroup.rehabcenter.model.Patient;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService{

    private PatientDao employeeDao;
    private AppointmentDao appointmentDao;
    private DiagnosisDao diagnosisDao;

    @Autowired
    public void setEmployeeDao(PatientDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Autowired
    public void setAppointmentDao(AppointmentDao appointmentDao) {
        this.appointmentDao = appointmentDao;
    }
    @Autowired
    public void setDiagnosisDao(DiagnosisDao diagnosisDao) {
        this.diagnosisDao = diagnosisDao;
    }

    @Override
    @Transactional
    public List<Patient> allPatients(int page) {
        return employeeDao.allPatients(page);
    }

    @Override
    @Transactional
    public List <Patient> allPatientsByDoctorsId(int id) {
        return employeeDao.allPatientsByDoctorsId(id);
    }

    @Override
    @Transactional
    public void add(Patient patient) {
        employeeDao.add(patient);
    }

    @Override
    @Transactional
    public void delete(Patient patient) {
        employeeDao.delete(patient);
    }

    @Override
    @Transactional
    public void edit(Patient patient) {
        employeeDao.edit(patient);
    }

    @Override
    @Transactional
    public Patient getById(int id) {
        return employeeDao.getById(id);
    }

    @Override
    @Transactional
    public int patientsCount() {
        return employeeDao.patientsCount();
    }
    @Override
    @Transactional
    public List<Appointment> getAppointments(int id){
        return appointmentDao.getAllFromPatientId(id);
    }

    @Override
    @Transactional
    public List <Diagnosis> getDiagnosis(int id) {
        return diagnosisDao.getAllFromPatientId(id);
    }
}
