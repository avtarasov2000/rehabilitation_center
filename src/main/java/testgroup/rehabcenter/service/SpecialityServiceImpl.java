package testgroup.rehabcenter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import testgroup.rehabcenter.dao.SpecialityDao;
import testgroup.rehabcenter.model.Speciality;

import java.util.List;

@Service
public class SpecialityServiceImpl implements SpecialityService{

    private SpecialityDao specialityDao;

    @Autowired
    public void setSpecialityDao(SpecialityDao specialityDao) {
        this.specialityDao = specialityDao;
    }


    @Override
    @Transactional
    public List<Speciality> allSpecialitys() {
        return specialityDao.allSpecialitys();
    }

    @Override
    @Transactional
    public void add(Speciality speciality) {
        specialityDao.add(speciality);
    }

    @Override
    @Transactional
    public void delete(Speciality speciality) {
        specialityDao.delete(speciality);
    }

    @Override
    @Transactional
    public void edit(Speciality speciality) {
        specialityDao.edit(speciality);
    }

    @Override
    @Transactional
    public Speciality getById(int id) {
        return specialityDao.getById(id);
    }

    @Override
    @Transactional
    public Speciality getByName(String name) {
        return specialityDao.getByName(name);
    }
}
