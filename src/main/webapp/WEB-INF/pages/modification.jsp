<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>modification</title>
    <link href="<c:url value="/resources/Stylesheet.css"/>" rel="stylesheet" type="text/css"/>
</head>



<body>




<div id="tablet">

    <div id="upperField">
        <h1 style="text-align: center"><a href="/">реабилитационный центр</a></h1>
    </div>


    <div id="information">
        <h1>101</h1>



        <c:if test="${empty employee.name}">
            <c:url value="/add" var="var"/>
        </c:if>

        <c:if test="${!empty employee.name}">
            <c:url value="/edit" var="var"/>
        </c:if>

        <form action="${var}" method="POST">

            <c:if test="${!empty employee.name}">
                <input type="hidden" name="id" value="${employee.id}">
            </c:if>

            <c:if test="${empty employee.name}">
                <input type="hidden" name="id" value=0>
            </c:if>

                <label for="name">Name</label>
                <input type="text" name="name" id="name" value="${employee.name}">

                <label for="surname">Surname</label>
                <input type="text" name="surname" id="surname" value="${employee.surname}">

                <label for="secondName">Second Name</label>
                <input type="text" name="secondName" id="secondName" value="${employee.secondName}">

                <label for="birthday">Birthday</label>
                <input type="text" name="birthday" id="birthday" value="${employee.birthday}">


            <c:if test="${empty employee.name}">
            <input type="submit" value="Add new employee">
            </c:if>

            <c:if test="${!empty employee.name}">
            <input type="submit" value="Edit employee">
            </c:if>

        </form>
    </div>

</div>

</body>
</html>
