<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>patients</title>
    <link href="<c:url value="/resources/Stylesheet.css"/>" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="tablet">

    <div id="upperField">
        <h1 style="text-align: center">реабилитационный центр</h1>
    </div>


    <div id="information">
        <h1>101</h1>
        <table>
            <tr>
                <th>name</th>
                <th>surname</th>
                <th>second name</th>
                <th>insurance</th>
                <th>appointments</th>
                <th>diagnoses</th>

            </tr>
            <c:forEach var="patients" items="${patients}">
                <tr>
                    <td>${patients.name}</td>
                    <td>${patients.surname}</td>
                    <td>${patients.secondName}</td>
                    <td>${patients.insurance}</td>
                    <td><a href="/appointments/${patients.id}">appointments</a></td>
                    <td><a href="/diagnosis/${patients.id}">diagnoses</a></td>
                </tr>
            </c:forEach>


        </table>


        <h2>add</h2>
        <c:url value="/add" var="add"/>
        <a href="${add}">Add new employee</a>
    </div>

    <%--<c:forEach begin="1" end="${pagesCount}" step="1" varStatus="i">--%>
        <%--<c:url value="/" var="url">--%>
            <%--<c:param name="page" value="${i.index}"/>--%>
        <%--</c:url>--%>
        <%--<a href="${url}">${i.index}</a>--%>
    <%--</c:forEach>--%>
    <%--<h3><a href="/login">login</a></h3>--%>
</div>
</body>
</html>
